import {
  createGlobalStyle,
  css,
  DefaultTheme,
  GlobalStyleComponent
} from 'styled-components'

// eslint-disable-next-line @typescript-eslint/ban-types
type GlobalStylesProps = {}

const GlobalStyles: GlobalStyleComponent<
  GlobalStylesProps,
  DefaultTheme
> = createGlobalStyle`

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    &::before,
    &::after {
      box-sizing: inherit;
    }
  }

  ${({ theme }) => css`
    html {
      font-size: 62.5%;
    }

    body {
      font-family: ${theme.font.family};
      font-size: ${theme.font.sizes.size16};
      background-color: ${theme.colors.background};
      color: ${theme.colors.text};
    }

    body, input, textarea, button {
      font-family: ${({ theme }) => theme.font.regular};
      font-size: ${({ theme }) => theme.font.sizes.size16}
      font-weight: 400;
      outline: none;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    input[type="number"] {
      -moz-appearance: textfield;
    }

    button {
      cursor: pointer;
      border: none;
    }

    a {
      text-decoration: none;
    }

    ::-webkit-scrollbar {
      width: 0.25rem;
    }

    ::-webkit-scrollbar-track {
      background: ${({ theme }) => theme.colors.background}
    }

    ::-webkit-scrollbar-thumb {
      border-radius: 2rem;
      background: ${({ theme }) => theme.colors.blue}
    }
  `}
`
export default GlobalStyles
