import styled from 'styled-components'

export const Wrapper = styled.main``

export const Posts = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 3.2rem;
  margin: 4.8rem auto;
`
