import { Container } from '../../components/Container'
import { PostItem } from '../../components/PostItem'
import { Profile } from '../../components/Profile'
import { Search } from '../../components/Search'

import * as S from './styles'

export const Home = () => {
  return (
    <Container>
      <S.Wrapper>
        <Profile />

        <Search />

        <S.Posts>
          <PostItem />
          <PostItem />
          <PostItem />
          <PostItem />
          <PostItem />
          <PostItem />
          <PostItem />
        </S.Posts>
      </S.Wrapper>
    </Container>
  )
}
