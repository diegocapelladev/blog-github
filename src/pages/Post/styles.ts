import styled, { css } from 'styled-components'

export const Wrapper = styled.div``

export const PostContent = styled.div`
  ${({ theme }) => css`
    padding: 4rem 3.2rem;
    font-size: ${theme.font.sizes.size16};
    color: ${theme.colors.text};

    p {
      margin-bottom: 2rem;

      span {
        font-weight: bold;
      }
    }

    a {
      text-decoration: underline;
    }

    div {
      padding: 1.6rem;
      background: ${theme.colors.bgCard};
      border-radius: 0.2rem;

      code {
        line-height: 1.6;
        display: block;
      }
    }
  `}
`
