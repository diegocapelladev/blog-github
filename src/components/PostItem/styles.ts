import styled, { css } from 'styled-components'
import { Link } from 'react-router-dom'

export const Wrapper = styled(Link)`
  ${({ theme }) => css`
    background: ${theme.colors.bgCard};
    border-radius: ${theme.border.radius};
    padding: 3.2rem;
    text-decoration: none;
    color: ${theme.colors.text};
  `}
`
export const PostTitle = styled.div`
  display: flex;
  align-items: stretch;
  justify-content: space-between;
  margin-bottom: 2rem;

  ${({ theme }) => css`
    h2 {
      font-size: ${theme.font.sizes.size20};
      color: ${theme.colors.title};
    }

    span {
      min-width: fit-content;
      font-size: ${theme.font.sizes.size14};
      color: ${theme.colors.dataTime};
    }
  `}
`
