import { Buildings, GitlabLogo, Share, Users } from 'phosphor-react'
import { Link } from 'react-router-dom'
import * as S from './styles'

export const Profile = () => (
  <S.Wrapper>
    <img
      src="https://gitlab.com/uploads/-/system/user/avatar/11804551/avatar.png"
      alt=""
    />
    <S.Content>
      <S.ProfileName>
        <h2>Diego Capella</h2>
        <Link to="/">
          Gitlab <Share size={20} />
        </Link>
      </S.ProfileName>

      <S.Description>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus
        officia, dolorem maxime aliquam ipsa odio! Lorem ipsum dolor sit amet
        consectetur, adipisicing elit. Doloribus officia, dolorem maxime aliquam
        ipsa odio!
      </S.Description>

      <S.Details>
        <p>
          <GitlabLogo size={20} />
          diegocapelladev
        </p>

        <p>
          <Buildings size={20} />
          CloudOpss
        </p>

        <p>
          <Users size={20} />0 seguidores
        </p>
      </S.Details>
    </S.Content>
  </S.Wrapper>
)
