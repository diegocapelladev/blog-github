import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  width: 100%;
  margin-top: 7.2rem;
`
export const SearchTitle = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${({ theme }) => css`
    h2 {
      font-size: ${theme.font.sizes.size18};
      color: #C4D4E3;
    }

    p {
      color: ${theme.colors.dataTime};
    }
  `}
`
export const Form = styled.form`
  margin-top: 1.2rem;

  input {
    width: 100%;
    height: 5rem;
    border-radius: 0.6rem;
    border: 1px solid #1C2F41;
    padding: 1.2rem 1.6rem;

    ${({ theme }) => css`
      color: ${theme.colors.text};
      background: ${theme.colors.input};

      &::placeholder {
        color: ${theme.colors.icon};
        font-size: ${theme.font.sizes.size16};
      }

      &:focus {
        border: 1px solid ${theme.colors.blue};
      }
    `}
  }
`
