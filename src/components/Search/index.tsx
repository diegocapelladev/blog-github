import * as S from './styles'

export const Search = () => (
  <S.Wrapper>
    <S.SearchTitle>
      <h2>Publicações</h2>
      <p>
        <span>0</span> publicações
      </p>
    </S.SearchTitle>

    <S.Form>
      <input type="text" placeholder="Buscar conteúdo" />
    </S.Form>
  </S.Wrapper>
)
