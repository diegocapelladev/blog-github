import styled from 'styled-components'

const Wrapper = styled.header`
  background-image: url('images/cover.png');
  background-size: cover;
  background-position: center;
  background-repeat: repeat;
  height: 296px;
`

export const Header = () => <Wrapper />
