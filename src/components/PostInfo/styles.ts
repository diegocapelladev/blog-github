import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    margin-top: -8rem;
    background: ${theme.colors.bgProfile};
    border-radius: ${theme.border.radius};
    padding: 4rem;
    display: flex;
    gap: 3.2rem;

    img {
      border-radius: ${theme.border.radius};
      width: 14.8rem;
      height: 14.8rem;
    }
  `}
`
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

export const Links = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: space-between;


    a {
      color: ${theme.colors.blue};
      font-weight: 700;
      text-transform: uppercase;
      display: flex;
      align-items: center;
      gap: 0.8rem;
    }
  `}
`

export const PostTitle = styled.h2`
  ${({ theme }) => css`
    margin-top: 2rem;
    margin-bottom: 1rem;
    color: ${theme.colors.title};
    font-size: ${theme.font.sizes.size24};
  `}
`

export const Details = styled.div`
  display: flex;
  gap: 2.4rem;
  margin-top: auto;

  p {
    ${({ theme }) => css`
      display: flex;
      align-items: center;
      gap: 0.8rem;
      font-size: ${theme.font.sizes.size16};

      svg {
        color: ${theme.colors.icon};
      }
    `}
  }
`
