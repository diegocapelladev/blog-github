import {
  Calendar,
  CaretLeft,
  ChatCircle,
  GitlabLogo,
  Share
} from 'phosphor-react'
import { Link } from 'react-router-dom'
import * as S from './styles'

export const PostInfo = () => (
  <S.Wrapper>
    <S.Content>
      <S.Links>
        <Link to="/">
          <CaretLeft size={20} /> Voltar
        </Link>

        <Link to="/">
          ver no Gitlab <Share size={20} />
        </Link>
      </S.Links>

      <S.PostTitle>JavaScript data types and data structures</S.PostTitle>

      <S.Details>
        <p>
          <GitlabLogo size={20} />
          diegocapelladev
        </p>

        <p>
          <Calendar size={20} />
          Há 1 dia
        </p>

        <p>
          <ChatCircle size={20} />0 comentários
        </p>
      </S.Details>
    </S.Content>
  </S.Wrapper>
)
